import React, {Component} from 'react';
import GooglePayButton from '@google-pay/button-react';

const paymentRequest = {
    apiVersion: 2,
    apiVersionMinor: 0,
    allowedPaymentMethods: [
        {
            type: 'CARD',
            parameters: {
                allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
                allowedCardNetworks: ["AMEX", "DISCOVER", "MASTERCARD", "VISA"],
            },
            tokenizationSpecification: {
                type: 'PAYMENT_GATEWAY',
                parameters: {
                    gateway: 'example',
                    gatewayMerchantId: 'exampleGatewayMerchantId',
                },
            },
        },
    ],
    merchantInfo: {
        merchantId: '12345678901234567890',
        merchantName: 'Example Merchant'
    },
    transactionInfo: {
        displayItems: [
            {
                label: "Subtotal",
                type: "SUBTOTAL",
                price: "11.00",
            },
            {
                label: "Tax",
                type: "TAX",
                price: "1.00",
            }
        ],
        countryCode: 'GB',
        currencyCode: "GBP",
        totalPriceStatus: "FINAL",
        totalPrice: "12.00",
        totalPriceLabel: "Total"
    },
};

class GooglePay extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };

    handleLoadPaymentData = (paymentData) => {
        console.log('load payment data', paymentData);
    }

    handleCancelPaymentData = (paymentData) => {
        console.log(paymentData)
    }

    handleErrorPaymentData = (paymentData) => {
        console.log(paymentData)
    }

    handleAuthPaymentData = (paymentData) => {
        console.log(paymentData)
    }

    handlePaymentDataChanged = (paymentData) => {
        console.log(paymentData)
    }

    handlePaymentReadyToPay = (paymentData) => {
        console.log(paymentData)
    }

    render() {
        return (
            <div>
                <GooglePayButton
                    environment="TEST"
                    buttonType="buy"
                    paymentRequest={paymentRequest}
                    onLoadPaymentData={this.handleLoadPaymentData}
                    // onCancel={this.handleCancelPaymentData}
                    // onError={this.handleErrorPaymentData}
                    // onPaymentAuthorized={this.handleAuthPaymentData}
                    // onPaymentDataChanged={this.handlePaymentDataChanged}
                    // onReadyToPayChange={this.handlePaymentReadyToPay}
                />
            </div>
        );
    };
}

export default GooglePay;