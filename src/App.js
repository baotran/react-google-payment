import logo from './logo.svg';
import './App.css';
import GooglePay from "./GooglePay";

function App() {
    return (
        <div>
            <GooglePay/>
        </div>
    );
}

export default App;
